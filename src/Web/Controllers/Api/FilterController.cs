using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web.Models;

namespace Web.Controllers.Api
{
    [Route("api/[controller]")]
    public class FilterController : Controller
    {
        [HttpPost("FilterShows")]
        public IActionResult FilterShows([FromBody] Newtonsoft.Json.Linq.JObject paramObj = null)
        {
            var error = new { Error = "Could not decode request: JSON parsing failed" };
            if (paramObj == null) { return BadRequest(error); }
            
            try
            {
                var filteredPayload = paramObj["payload"]
                    .Where(item =>
                        item["drm"] != null && (bool)item["drm"] == true && 
                        item["episodeCount"] != null && (int)item["episodeCount"] > 0
                    )
                    .Select(item => new { Image = (string)item["image"]["showImage"], Slug = (string)item["slug"], Title = (string)item["title"] });
                JsonConvert.SerializeObject(filteredPayload); // Check to make sure this filteredPayload JSON is valid.

                return Ok(new { Response = filteredPayload });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
            return BadRequest(error);
        }
    }
}
