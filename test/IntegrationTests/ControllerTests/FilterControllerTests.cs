using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Web;
using Xunit;

namespace IntegrationTests.ControllerTests
{
    public class FilterControllerTests
    {
        private readonly TestServer _server;
        private readonly HttpClient _client;
        public FilterControllerTests()
        {
            // Arrange
            _server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>());
            _client = _server.CreateClient();
        }

        [Fact]
        public async Task PostDataReturnSuccess()
        {
            try
            {
                // Arrange.
                var show1 = new { country = "UK", drm = true, episodeCount = 3, image = new { showImage = "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg" }, slug = "show/16kidsandcounting", title = "16 Kids and Counting" };
                var show2 = new { country = "USA", drm = true, episodeCount = 2, image = new { showImage = "http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg" }, slug = "show/thetaste", title = "The Taste" };
                var showList = new[] { show1, show2 };
                var sampleData = new { payload = showList, skip = 0, take = 10, totalRecords = 75 };

                // Act
                var response = await _client.PostAsync("api/filter/filtershows", new JsonContent(sampleData));
                var returnedData = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());
                JObject jsonObj = JObject.Parse(JsonConvert.SerializeObject(returnedData));

                // Assert
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                Assert.True(jsonObj["response"].Any());
            }
            catch (Exception ex)
            {
                Assert.Null(ex);
            }
        }

        [Fact]
        public async Task PostInvalidDataReturnError()
        {
            try
            {
                // Arrange.
                var show1 = new { country = "UK", drm = "", episodeCount = 3, image = new { showImage = "http://mybeautifulcatchupservice.com/img/shows/16KidsandCounting1280.jpg" }, slug = "show/16kidsandcounting", title = "16 Kids and Counting" };
                var show2 = new { country = "USA", drm = "", episodeCount = 2, image = new { showImage = "http://mybeautifulcatchupservice.com/img/shows/TheTaste1280.jpg" }, slug = "show/thetaste", title = "The Taste" };
                var showList = new[] { show1, show2 };
                var sampleData = new { payload = showList, skip = 0, take = 10, totalRecords = 75 };

                // Act
                var response = await _client.PostAsync("api/filter/filtershows", new JsonContent(sampleData));
                var returnedData = JsonConvert.DeserializeObject(await response.Content.ReadAsStringAsync());
                JObject jsonObj = JObject.Parse(JsonConvert.SerializeObject(returnedData));

                // Assert
                Assert.Equal(HttpStatusCode.BadRequest, response.StatusCode);
                Assert.NotNull(jsonObj["error"]);
            }
            catch (Exception ex)
            {
                Assert.Null(ex);
            }
        }
    }

    public class JsonContent : StringContent
    {
        public JsonContent(object obj) :
            base(JsonConvert.SerializeObject(obj), Encoding.UTF8, "application/json")
        { }
    }
}
